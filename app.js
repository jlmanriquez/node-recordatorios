const argv = require('./config/yargs').argv;
const porHacer = require('./por-hacer/por-hacer');
const colors = require('colors');

let comando = argv._[0];

switch (comando) {
    case 'crear':
        let tarea = porHacer.crear(argv.descripcion);
        console.log(tarea);
        break;
    case 'listar':
        let tareas = porHacer.getListar();
        for (let tarea of tareas) {
            console.log('======= Por Hacer ======='.green);
            console.log(tarea.descripcion);
            console.log('Estado: ', tarea.completado);
            console.log('========================='.green);
        }
        break;
    case 'actualizar':
        let actualizada = porHacer.actualizar(argv.descripcion, argv.completado);
        console.log('Tarea actualizada: ', actualizada);
        break;
    case 'borrar':
        let borrada = porHacer.borrar(argv.descripcion);
        console.log('Tarea borrada: ', borrada);
        break;
    default:
        console.log('Comando no reconocido');
}