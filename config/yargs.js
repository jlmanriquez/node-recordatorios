const descripcion = {
    demand: true,
    alias: 'd',
    desc: 'Descripción de la tarea por hacer'
};

const completado = {
    demand: false,
    alias: 'c',
    default: 'true',
    desc: 'Actualiza el estado de la tarea'
};

// En ECMAScript 6 cuando el nombre de la propiedad es igual al valor por ejemplo
// { descripcion: descripcion } se permite hacer { descripcion }
const argv = require('yargs')
    .command('crear', 'Crear un elemento por hacer', {
        descripcion
    })
    .command('actualizar', 'Actualiza el estado completado de una tarea', {
        descripcion,
        completado
    })
    .command('borrar', 'Borra un registro de las tareas por hacer', {
        descripcion
    })
    .help()
    .argv;

module.exports = {
    argv
}