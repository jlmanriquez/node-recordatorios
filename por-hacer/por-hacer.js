const fs = require('fs');

const dbName = 'db/data.json';

let listadoPorHacer = [];

// Crea una nueva tarea por hacer
const crear = (descripcion) => {
    cargarDB();

    let porHacer = {
        descripcion,
        completado: false
    }

    listadoPorHacer.push(porHacer);
    guardarDB();
    return porHacer;
}

const getListar = () => {
    cargarDB();
    return listadoPorHacer;
}

const guardarDB = () => {
    let data = JSON.stringify(listadoPorHacer);
    fs.writeFileSync(dbName, data);
}

const cargarDB = () => {
    try {
        listadoPorHacer = require('../db/data.json');
    } catch (error) {
        listadoPorHacer = [];
    }
}

const actualizar = (descripcion, completado = true) => {
    cargarDB();

    let index = listadoPorHacer.findIndex(tarea => tarea.descripcion === descripcion);
    if (index >= 0) {
        listadoPorHacer[index].completado = completado;
        guardarDB();
        return true;
    } else {
        return false;
    }
}

const borrar = (descripcion) => {
    cargarDB();
    let oldLength = listadoPorHacer.length;
    listadoPorHacer = listadoPorHacer.filter(tarea => tarea.descripcion !== descripcion);
    guardarDB();
    return (listadoPorHacer.length < oldLength);
}

module.exports = {
    crear,
    getListar,
    actualizar,
    borrar
}